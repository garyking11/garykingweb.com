<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<?php include 'includes/head.php'; ?>
<body>

<!--<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>-->

<!-- start slider -->

<div class="slider .visible-xs-block, hidden-xs .visible-sm-block, hidden-sm">

    <div>
        <img src="assets/images/slides/slider2.jpg" alt="Slide"/>

        <div class="">
            <h3 class="h2-section-title text-center">RESPONSIVE WEB DEV</h3>
        </div>
    </div>

    <div>
        <img src="assets/images/slides/slide-04.jpg" alt="Slide"/>

        <div class="">
            <h3 class="h2-section-title  text-center">Designs using the latest technologies!</h3>

        </div>
    </div>
    <div>
        <img src="assets/images/slides/slider7.jpg" alt="Slide"/>
        <div class="">
            <h3 class="h2-section-title text-center">Angular > LESS > CSS > HTML5 </h3>
        </div>
    </div>

</div>

<!-- end slider -->


<header class="bg-header">
    <div class="container">
        <div class="hd-col-left">
            <img src="/assets/images/gary-photo1.jpeg" alt="photo of Gary"/>
        </div>
        <div class="hd-col-left-logo">
            <div class="logo-text wow slideInDown">Gary King</div>
            <section class="wow fadeIn">
                <strong>I am a Seattle-based Web Developer with more than 15 years professional experience.</strong>
                <p>I have been working with and am currently employed by Sesame Communications Inc. as a Senior Web Developer for the past 8 years. We develop web solutions
                    for the dental and orthodontic industry. I have personally built
                    hundreds of websites for dentists and orthodontists. In addition, I have created web development tools and local environment tools for the web-dev team using the Angular javascript framework (see below for details).</p>
        </div>

    </div>
</header>
<hr />
<div class="container">
    <div class="row">

        <div class="col-md-7 col-sm-7">

                <div class="left-title">
                    <div class="space-sep40"></div>
                    <div class="content-box  style5  mauve animated "
                         data-animtype="fadeIn"
                         data-animrepeat="0"
                         data-animspeed="1s"
                         data-animdelay="0.2s">

                        <h4 class="h4-body-title">
                            Front end &amp; back end development
                            <i class="icon-gears"></i>
                        </h4>

                        <div class="content-box-text">
                            <p>HTML5, CSS3, LESS, SASS, PHP, Angular [1, 2, 4, 5], NodeJs, Express, Adobe Creative Suite
                                and jQuery. </p>
                        </div>
                    </div>


                    <div class="content-box  style5  mauve animated "
                         data-animtype="fadeIn"
                         data-animrepeat="0"
                         data-animspeed="1s"
                         data-animdelay="0.2s"
                    >
                        <h4 class="h4-body-title">
                            Responsive Site Development
                            <i class="icon-tablet"></i>
                        </h4>

                        <div class="content-box-text">

                            <p>Recent projects creating online products:</p>
                            <ul class="icon-content-list-container">

                                <li><strong>Developed the Site Manager, a web development local environment and toolkit
                                        for
                                        <br/>
                                        <i class="icon-circle-arrow-right"></i>&nbsp
                                        <a href="https://www.sesamecommunications.com/web-design-gallery/#sds"
                                           target="_blank">
                                            Sesame Responsive Web Design templates</a></strong>
                                    <br/>(HTML5, CSS3, LESS, SASS, jQuery, Angular [1, 2, 4, 5], PHP)
                                    <br/><br/>
                                    <div>
                                        <strong>Including the following tool modules:</strong><br/>
                                        Color Styler<br/>
                                        301 Redirect Generator<br/>
                                        HTML/CSS page generator for responsive site builds<br/>
                                        SEO Schema Generator<br/>
                                        Font Manager<br/>
                                        Image Manager<br/>
                                        "Color My Braces"<br/>
                                    </div>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <div class="content-box  style5  mauve animated "
                         data-animtype="fadeIn"
                         data-animrepeat="0"
                         data-animspeed="1s"
                         data-animdelay="0.2s">

                    </div>

                </div>

            </section>


        </div>

        <div class="col-md-5 col-sm-5 wow slideInRight">

            <h2>Technology</h2>

            <p>HTML5
                <br/>AJAX
                <br/>jQuery
                <br/>Angular [1, 2, 4, 5]
                <br/>CSS3
                <br/>LESS
                <br>SASS
                <br/>PHP
                <br/>JavaScript
                <br/>Express
            </p>

            <h3>Concepts</h3>

            <p> OOP
                <br/>Responsive Design
                <br/>jQuery Animations
                <br/>PSD Wireframes
                <br/>Landing Pages
                <br/>UI/UX
                <br/>W3C Compliancy
                <br/>CMS
                <br/>Web Accessibility
                <br/>Standards Compliance
            </p>

            <h3>Version Control</h3>
            <p>
                GitHub
                <br/>Bitbucket
            </p>

        </div>

        <hr/>

    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<?php include 'includes/footer.php'; ?>

</body>
</html>