    <!-- Header -->
    

    <header id="header">
        <div class="container">

            <div class="row header">

                <!-- Logo -->
                <div class="col-md-4 col-sm-4 col-xs-4 logo">
                    
                     <div class="center-title">
                    
                    <h1><a href="index.php">GARY KING <span>Web Developer</span></a></h1>
                   
                </div>
                    
                    </div>


                <!-- Navigation File -->
                <div class="col-md-8">

                    <!-- Mobile Button Menu -->
                    <div class="mobile-menu-button">
                        <i class="icon-reorder"></i>
                    </div>
                    <!-- //Mobile Button Menu// -->


                      <nav>
                        <ul class="navigation">
                            <li>
                                <a href="index.php">
                                    <span class="label-nav">
                                        Home
                                    </span>
                                </a>                               
                            </li>
                            <li>
                                <a href="skills.php">
                                    <span class="label-nav">
                                        Skills
                                    </span>
                                </a>

                            </li>
  
                            <li>
                                <a href="examples.php">
                                    <span class="label-nav">
                                        Examples
                                    </span>
                                </a>
                                
                            </li>
  
                            <li>
                                <a href="testimonials.php">
                                    <span class="label-nav">
                                        Testimonials
                                    </span>
                                </a>
                               
                            </li>
                            
                           <li>
                                <a href="contact.php">
                                    <span class="label-nav">
                                        Contact
                                    </span>
                                </a>
                                
                            </li>                            
                        </ul>

                    </nav>
                    
                    
                   <!-- Mobile Nav. Container -->
                    <ul class="mobile-nav">
                        <li class="responsive-searchbox">
                            <!-- Responsive Nave -->
                            <!--<form action="#" method="get">
                                <input type="text" class="searchbox-inputtext" id="searchbox-inputtext-mobile" name="s" />
                                <button class="icon-search"></button>
                            </form>-->
                            <!-- //Responsive Nave// -->
                        </li>
                    </ul>
                    <!-- //Mobile Nav. Container// -->
                </div>
                <!-- Nav -->
                
                </div>
                <!-- Nav -->

            </div> 
            
        
        
    </header>
    <!-- //Header// -->